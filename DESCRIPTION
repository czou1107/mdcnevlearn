Package: MDCNEVlearn
Type: Package
Title: Estimate MDCNEV Model with Bayesian Learning
Version: 0.3.0
Authors@R:
    person("Charles", "Zou", email = "czou1107@gmail.com", role = c("aut", "cre"))
Description: This package contains a set of functions to describe and estimate various
    structural marketing models, from basic single discrete-choice to the multiple discrete-
    continuous nested extreme value (MDCNEV) model with Bayesian learning introduced in my
    PhD thesis (Zou, 2019). The models are implemented using S4 classes, and estimation
    leverages optimization routines from nloptr.
License: GPL-3 | file LICENSE
URL: tbd
Encoding: UTF-8
LazyData: true
Imports:
    methods (>= 3.5.3),
	nloptr (>= 1.2.1),
	abind (>= 1.4.5),
	copula (>= 0.999.19)
Suggests:
    Matrix,
    rmarkdown,
    knitr,
    testthat,
	ggplot2
Depends:
    R (>= 3.5.3)
RoxygenNote: 6.1.1
VignetteBuilder: knitr
Collate: 
    'MDCNEVlearn.R'
    'utils.R'
    'imports.R'
    'def_S4_Model.R'
    'def_S4_Data.R'
    'utils_S4.R'
    'def_S4_DataModelValidation.R'
    'def_S4_Estimation.R'
    'def_S4_Likelihood.R'
    'def_S4_Simulation.R'
    'zzz.R'
