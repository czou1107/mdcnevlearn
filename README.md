
MDCNEVlearn
===========

Overview
--------

This package contains an object-oriented (S4) framework for estimating various structural quantitative marketing models, ranging from static models of single discreteness to the multiple discrete-continuous nested extreme error (MDCNEV) model with Bayesian learning studied in my Phd thesis. This code is an adapted slice of the complete, end-to-end repository I composed to munge raw data, perform visualizations, iterate endlessly through simulations and estimations, and summarize my results in various tables and plots. In my original work estimation code was written in Matlab and an expensive, proprietary solver was used. Here I rely entirely on open-source software, and gladly welcome any comments or suggestions, from design to engineering.

Installation
------------

You can install the development version of MDCNEVlearn from [bitbucket](https://bitbucket.org) with:

``` r
# install.packages("devtools")
devtools::install_bitbucket("czou1107/mdcnevlearn")
```

Example
-------

The core of this package is a Model object, which completely defines a parametric model of consumer choice. It can be used to simulate data from the implied choice/usage distribution, or used as a skeleton in conjunction with a Data object, to estimate model parameters. Sample usage follows:

``` r
library(MDCNEVlearn)
#> MDCNEVlearn version 0.3.0
#> Please contact Zou, Charles (czou@chicagobooth.edu) with questions.
# Define a model of Bayesian learning with single discreteness, i.e. consumer utility:
(simpleModel <- ModelSDlearn(coefficients = list(
  beta_v = c(lprice = -0.3, feat = 0.1), # Utility drivers: marginal utility for lprice, feat.
  qprior_c = c(1, -1), # Prior mean belief for each of the 2 goods. This is the intercept.
  qprior_v = c(lage = 0.2), # Prior mean beliefs are influenced by lage.
  qtrue_c = c(-3, -3), # True means are homogeneous.
  svar_c = c(3, 2) # Consumption signals are noisier for the first good, i.e. less informative.
)))
#> Model: SDlearn  (abstract). Utility defined as follows:
#> ----------
#> Eu(x_j)  ~ lprice + feat + E(Q_ijt) - ra:Var(Q_ijt)
#> ----------
#> beta     ~ lprice + feat
#> qprior   ~ (Intercept) + lage
#> qtrue    ~ (Intercept)
#> svar     ~ exp((Intercept))
#> ----------
#> Parameter values:
#> ----------
#> beta_v  : [lprice = -0.3, feat = 0.1]
#> qprior_c: [(Intercept) = 1, -1]
#> qprior_v: [lage = 0.2]
#> qtrue_c : [(Intercept) = -3, -3]
#> svar_c  : [(Intercept) = 3, 2]
#> ----------
#> Error distribution: i.i.d. Gumbel (T1EV)
# Use this model to simulate data:
# Here the dimensions of Z correspond to NT (user x period) x J (num. of goods) x M (attributes)
(simulatedData <- simulateData(simpleModel,
                               Z = array(rnorm(120), c(20, 2, 3)), # Randomly generate some covariates
                               I = rep(letters[1:4], each = 5), # Index consumers. Here there are 4: a,b,c,d
                               Z.varNames = c("lprice", "feat", "lage"), # Covariate names matching model
                               includeOutsideOption = TRUE
                               )) # Include no-purchase as an option
#> Object of class DataSim:
#>   N = 4 unique IDs (a, a, a, ...)
#>   NT = 20 total obs. of J = 2 inside goods (+ 1 outside)
#>   M = 3 observed attributes
#> ----------
#> Consumption X (20 x 3):
#>  X1 X2 X0
#>   0  0  1
#>   1  0  0
#>   0  0  1
#>   ...
#>   M = 3 attributes observed for each good
#> ----------
#> Attributes Z (20 x 2 x 3:)
#> lprice:
#>      X1     X2
#>  -0.475 -1.092
#>   1.074 -0.473
#>   1.997  1.979
#>   ...
#> feat:
#>      X1     X2
#>   1.399 -0.937
#>  -1.451 -1.482
#>  -0.917  0.407
#>   ...
#> lage:
#>      X1     X2
#>   0.791 -2.071
#>  -0.226  1.038
#>   0.105 -1.113
#>   ...
# Can then estimate the model. Should recover parameters:
# fittedModel <- estimateModel(simulatedData, simpleModel) # Not run.
```

For more comprehensive use cases and discussion of the models implemented here, please refer to the [vignette](https://czou1107.bitbucket.io/MDCNEVlearn_vignette.html).

References
----------

Zou, C. (2019). *Preference Discovery and Learning-by-doing in Video Game Play*. PhD thesis, University of Chicago Booth School of Business.
