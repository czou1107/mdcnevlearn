% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/def_S4_Model.R
\docType{class}
\name{ModelSkeleton-class}
\alias{ModelSkeleton-class}
\alias{show,ModelSkeleton-method}
\title{S4 virtual class for representation of parametric model.}
\usage{
\S4method{show}{ModelSkeleton}(object)
}
\description{
All models extend from this skeleton. It provides basic validity checks (e.g. lengths, requirements, syntax)
for coefficients.
}
\section{Methods (by generic)}{
\itemize{
\item \code{show}: Default show method for all Model objects
}}

