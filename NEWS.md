## version 0.3.0

---

- Vignette with toy example added (inst/doc). Basic plot method added for panel data.
- devtools::check() passed with some warnings (documentation related)

## version 0.2.0

---

- Documentation substantively complete. show methods complete. Ready to add to github

## version 0.1.0

---

- Model likelihoods (version 1) written up, estimation works

## version 0.0.0.9000

---

### setup

- added NEWS.md creation

