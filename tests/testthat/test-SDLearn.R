# Testing for SDlearn model
context("test-SDlearn\n")

# Test model creation
test_that('ModelSDlearn initializes correctly', {
  # 1. Can't miss any required parameters
  expect_error(ModelSDlearn(list(beta_c = 1:3, qprior_c = 0, svar_c = 12)), 'qtrue')
  # 2. qprior_c, qtrue_c, svar_c must be same length. Note, this doesn't extend to *_v
  expect_error(ModelSDlearn(list(beta_c = 1, qprior_c = 1:3, qtrue_c = 1:3, svar_c = 2:3)), 'same length')
  expect_error(ModelSDlearn(list(beta_c = 1, qprior_c = 1:3, qtrue_c = 1:3, svar_v = c(a = 1))), NA) # OK
})

# Test get/setters
test_that('ModelSDlearn getters/setters/validation', {
  numCons <- 2 # N
  panelLength <- 5 # T
  numGoods <- 2 # J
  numAttrib <- 4 # M

  bad_test_data <- DataSim(Z = rDistArray(rnorm, c(numCons * panelLength, numGoods, numAttrib)),
                           includeOutsideOption = TRUE,
                           Z.varNames = LETTERS[1:numAttrib])
  good_test_data <- DataSim(Z = rDistArray(rnorm, c(numCons * panelLength, numGoods, numAttrib)),
                            includeOutsideOption = TRUE,
                            Z.varNames = LETTERS[1:numAttrib],
                            I = rep(1:numCons, each = panelLength))
  # Data with J = 2, 4 attributes A..D
  correct_params <- list(
    beta_c = c(1, 3),
    qprior_c = c(-1, 1),
    qtrue_c = c(0, 0),
    svar_c = c(10, 10),
    beta_v = c(A = 1, B = 2, C = 3),
    qprior_v = c(D = 4, A = 0.1)
  )

  # Without explicit panels, check will fail for ALL learning models
  expect_error(validateDataModel(bad_test_data, ModelSDlearn(correct_params), skipLikelihoodCheck = TRUE),
               'too few observations')
  expect_error(validateDataModel(good_test_data, ModelSDlearn(correct_params), skipLikelihoodCheck = TRUE), NA)

  bad_params_1 <- bad_params_2 <- correct_params
  bad_params_1$beta_c <- rep(1, 5) # Too many constant pars
  expect_error(validateDataModel(good_test_data, ModelSDlearn(bad_params_1), skipLikelihoodCheck = TRUE),
               'beta_c')
  bad_params_2$qprior_v <- c(E = 0, A = 0.1) # Name is out of scope of Z's names
  expect_error(validateDataModel(good_test_data, ModelSDlearn(bad_params_2), skipLikelihoodCheck = TRUE),
               'qprior_v')

  # Test setters/getters
  test_model <- ModelSDlearn(list(
    beta_c = c(1),
    beta_v = c(A = 1, B = 2),
    qprior_c = 1:2,
    qprior_v = c(C = -1, D = 1),
    qtrue_c = 3:4,
    svar_c = rep(10, 2)
  ))
  # 1. Getters
  expect_error(getCoefs(test_model, c('beta', 'alpha')), 'invalid parameter')
  expect_equal(getCoefs(test_model), test_model@coefficients)
  expect_equal(getCoefs(test_model, c('beta', 'qtrue_c')),
               list(beta_c = 1, beta_v = c(A = 1, B = 2), qtrue_c = 3:4))
  # 2. Setters
  expect_error(setCoefs(test_model, list(beta_v = 0)), 'change model size')
  expect_error(setCoefs(test_model, list(qprior = 'a')), 'non-numerics')
  expect_warning(setCoefs(test_model, list(alpha = 1)), 'don\'t exist')
  expect_equal(setCoefs(test_model, 1:11)@coefficients,
               list(
                 beta_c = 1,
                 beta_v = c(A = 2, B = 3),
                 qprior_c = 4:5,
                 qprior_v = c(C = 6, D = 7),
                 qtrue_c = 8:9,
                 svar_c = 10:11
               ))

})

# Test that the cube works on a very simple example:
test_that('Learning cube works (not likelihood yet)', {
  # Test of equality:
  R <- 2
  X <- cbind(c(0,0), c(0,1), c(1,1)) # Corresponds to consumptions of each good
  pvar <- c(10, 20, 50)
  svar <- c(15, 15, 10)
  qprior <- c(-1, 0, 1)
  qtrue <- c(10, 5, 10)

  J <- ncol(X)
  T_ii <- nrow(X)
  # Method 1: Hard coded:
  err_draws <- 1:12
  # In the cube method, the correspondence is:
  # All periods, good 1, draw 1 -> All t, good 2, draw 1 ... --> All t, good J, draw 1 --> All t, good 1, draw 2
  V1 <- matrix(pvar, T_ii + 1, J, byrow = TRUE)
  V1[2, ] <- 1/(1/V1[1, ] + X[1, ]/svar)
  V1[3, ] <- 1/(1/V1[2, ] + X[2, ]/svar)

  Q1 <- aperm(array(qprior, c(J, T_ii + 1, R)), c(2, 1, 3)); tjr <- 1
  for (rr in 1:R) {
    for (jj in 1:J) {
      for (tt in 1:T_ii) {
        if (X[tt,jj] > 0) {
          Q1[tt+1,jj,rr] <- V1[tt+1,jj] * (Q1[tt,jj,rr]/V1[tt,jj] + err_draws[tjr]/svar[jj])
          tjr <- tjr + 1
        }
      }
    }
  }

  # Method 2: the cube
  NS <- rbind(0, X)
  TS <- sum(NS)
  CS <- apply(NS, 2, cumsum)
  Lambda <- 1/matrix(pvar, T_ii+1, J, byrow = TRUE) + CS/matrix(svar, T_ii+1, J, byrow = TRUE)
  V2 <- 1/Lambda

  Q0 <- aperm(array(qprior/pvar, c(J, T_ii + 1, R)), c(2, 1, 3)) # Baseline
  SIGVAR <- aperm(array(svar, c(J, T_ii + 1, R)), c(2, 1, 3))
  SIG <- array(0, dim(Q0))
  SIG[NS > 0] <- err_draws[1:6]/SIGVAR[NS > 0]
  CUMSIG <- apply(SIG, c(2,3), cumsum)
  Q2 <- c(V2) * (Q0 + CUMSIG)

  expect_equal(Q1, Q2)
})

# Test model likelihood
test_that('computeLoglikelihood.modelSDlearn works correctly', {
  # Setup
  numCons <- 1 # N
  panelLength <- 3 # T
  numGoods <- 1 # J
  numAttrib <- 2 # M

  test_data <- DataSim(Z = rDistArray(rnorm, c(numCons * panelLength, numGoods, numAttrib)),
                            includeOutsideOption = TRUE,
                            Z.varNames = LETTERS[1:numAttrib],
                            I = rep(1:numCons, each = panelLength))
  test_data@X <- rbind(
    c(1, 0),
    c(0, 1),
    c(0, 1)
    ##
  )

  # No risk aversion in this test
  test_model <- ModelSDlearn(list(
    beta_c = rep(1, numGoods),
    qprior_c = 1:numGoods,
    qprior_v = c(A = -1, B = 1),
    qtrue_c = rep(-1, numGoods),
    svar_c = rep(log(10), numGoods)
  ))

  expect_error(validateDataModel(test_data, test_model, TRUE), NA) # Ensure this is a valid structure

  # Likelihood by hand
  # Simulate learning
  .R <- 2
  seed_1 <- 04152019
  set.seed(seed_1); err_manual <- rnorm(sum(test_data@X[ ,numGoods] > 0) * .R)
  parList <- mapCoefsToWeights(test_data, test_model)
  svar1 <- parList[['svar']][1]
  Q1 <- matrix(parList[['qprior']], 1, .R)
  V1 <- 1

  V2 <- 1/(1/V1 + 1/svar1)
  Q2 <- V2 * (Q1/1 + matrix(parList[['qtrue']]/svar1, 1, .R) * 1 + err_manual * sqrt(1/svar1))

  V3 <- V2
  Q3 <- Q2

  Q <- rbind(Q1, Q2, Q3)
  draw1 <- cbind(parList[['beta']] + Q[ ,1], 0)
  draw2 <- cbind(parList[['beta']] + Q[ ,2], 0)

  prob1 <- prod(exp(draw1)[test_data@X > 0] / rowSums(exp(draw1)))
  prob2 <- prod(exp(draw2)[test_data@X > 0] / rowSums(exp(draw2)))

  totalProb <- -log(mean(c(prob1, prob2)))

  computeLoglikelihood(test_data, test_model, .R = 2, .seed_vec = seed_1)
})

# Test ability to recover parameters:
test_that('ModelSDlearn ability to recover parameters', {
  # Setup
  set.seed(20191215)
  numCons <- 20 # N
  panelLength <- 50 # T
  numGoods <- 2 # J
  numAttrib <- 1 # M

  # Quadratic RA in this test
  test_model <- ModelSDlearn(list(
    beta_v = setNames(seq(-1, 1, length.out = numAttrib), LETTERS[1:numAttrib]),
    qprior_c = 1:numGoods,
    qtrue_c = rep(-2, numGoods),
    svar_c = rep(log(10), numGoods),
    ra_c = log(0.5)
  ), .utilityForm = 'quadraticRiskAversionLinear')

  test_data <- simulateData(test_model,
                            Z = rDistArray(rnorm, c(numCons * panelLength, numGoods, numAttrib)),
                            includeOutsideOption = TRUE,
                            I = rep(1:numCons, each = panelLength),
                            Z.varNames = LETTERS[1:numAttrib])

  expect_error(validateDataModel(test_data, test_model, TRUE), NA) # Ensure this is a valid structure

  # Expect that with NEGATIVE learning, first half shares
  time_index <- 1:nrow(test_data@X) %% panelLength
  time_index[time_index == 0] <- time_index[time_index == 0] + panelLength
  time_groups <- cut(time_index, c(0, 25, 50))
  early_consumption <- colMeans(test_data@X[time_groups == levels(time_groups)[1], ])
  late_consumption <- colMeans(test_data@X[time_groups == levels(time_groups)[2], ])

  expect_gt(early_consumption[1], late_consumption[1])
  expect_gt(early_consumption[2], late_consumption[2])

  #computeLoglikelihood(test_data, test_model)
  npars <- length(getCoefs(test_model, doUnlist = TRUE))
  #computeLoglikelihood(test_data,
  #                   setCoefs(test_model, rnorm(npars)))
  if (getOption('MDCNEVlearn.skipLongTests')) skip('Skip expensive validation of parameter recovery in SDlearn')
  expect_lt({
    fitted_model <- estimateModel(test_data, test_model)
    ll_error <- rel_error(computeLoglikelihood(test_data, test_model),
                          computeLoglikelihood(test_data, fitted_model))
    par_error <- rel_error(getCoefs(test_model, doUnlist = TRUE),
                           getCoefs(fitted_model, doUnlist = TRUE),
                           p = Inf)
    ll_error
  }, 0.05)

})
