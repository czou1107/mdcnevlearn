# Here we define additional utility functions with methods
#' @include imports.R utils.R def_S4_Data.R def_S4_Model.R
NULL

# Map raw coefficients (with data) into weighted sums ---------------------
setGeneric('mapCoefsToWeights',
           function(.data, .model, ...) standardGeneric('mapCoefsToWeights'),
           signature = '.model')

setMethod('mapCoefsToWeights', 'ModelSkeleton', function(.data, .model, .applyFunParam = c(), ...){
  # General function that allows return of list of matrices, each of size NT, N, or 1 (defined in .applyFunParam)
  modelCoefs <- getCoefs(.model)
  paramNames <- names(modelCoefs)
  paramSuffixes <- paste0('_[', .model@.allowedTypes, ']$')
  uniqueParamGroups <- unique(sub(paramSuffixes, '', paramNames))
  outList <- setNames(vector('list', length(uniqueParamGroups)), uniqueParamGroups)
  # Expected output sizes
  NT <- nrow(.data@Z)
  uniqueCons <- unique(.data@I)
  N <- length(uniqueCons)
  idxFirst <- match(uniqueCons, .data@I)

  for (pg in uniqueParamGroups) {
    if (pg %in% names(.applyFunParam)) {
      switch(.applyFunParam[pg],
             'first' = {
               pg.index <- idxFirst
             },
             'constantVector' = {
               pg.index <- 1
               if (paste0(pg, '_v') %in% paramNames) stop(sprintf(
                 '.applyFunParam = \'constantVector\' requires %s_v not be set', pg
               ), call. = FALSE)
             },
             'constantScalar' = {
               pg.index <- 1
               if (paste0(pg, '_v') %in% paramNames) stop(sprintf(
                 '.applyFunParam = \'constantScalar\' requires %s_v not be set', pg
               ), call. = FALSE)
             },
             stop('.applyFunParam argument not recognized', call. = FALSE))
    } else pg.index <- 1:NT

    pg.sizeString <- .model@.requiredCoefs[pg]
    pg.size <- if (pg.sizeString == 'J') .data@J else as.numeric(pg.sizeString)

    if (paste0(pg, '_c') %in% paramNames) {
      out.pg <- matrix(modelCoefs[[paste0(pg, '_c')]], length(pg.index), pg.size, byrow = TRUE)
    } else out.pg <- matrix(0, length(pg.index), pg.size)

    if (paste0(pg, '_v') %in% paramNames) {
      par.pg <- modelCoefs[[paste0(pg, '_v')]]
      selectedVars <- match(names(par.pg), .data@Z.varNames)
      out.pg <- out.pg + einsteinSum(.data@Z[pg.index, ,selectedVars,drop=FALSE],
                                     matrix(par.pg, ncol = 1))
    }

    if (pg %in% names(.applyFunParam) && .applyFunParam[pg] == 'constantScalar') out.pg <- out.pg[1,1]
    outList[[pg]] <- out.pg
  }

  outList
})

## Additional methods should define .applyFunParam: which params should be not NT x size(param)
setMethod('mapCoefsToWeights', 'ModelSDlearn', function(.data, .model, .applyFunParam = c(), ...) {
  .applyFunParam <- c(.applyFunParam,
                      qprior = 'first',
                      qtrue = 'first',
                      svar = 'first',
                      ra = 'constantScalar')
  callNextMethod(.data, .model, .applyFunParam, ...)
})

setMethod('mapCoefsToWeights', 'ModelMDC', function(.data, .model, .applyFunParam = c(), ...) {
  .applyFunParam <- c(.applyFunParam) # Currently allow time-varying satiation and translation
  callNextMethod(.data, .model, .applyFunParam, ...)
})

setMethod('mapCoefsToWeights', 'ModelMDCNEV', function(.data, .model, .applyFunParam = c(), ...) {
  .applyFunParam <- c(.applyFunParam,
                      theta = 'constantScalar')
  callNextMethod(.data, .model, .applyFunParam, ...)
})

setMethod('mapCoefsToWeights', 'ModelMDCNEVlearn', function(.data, .model, .applyFunParam = c(), ...) {
  .applyFunParam <- c(.applyFunParam,
                      qprior = 'first',
                      qtrue = 'first',
                      svar = 'first',
                      ra = 'constantScalar')
  callNextMethod(.data, .model, .applyFunParam, ...)
})
