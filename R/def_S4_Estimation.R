# Here we define the functions needed to estimate our models
#' @include imports.R utils.R def_S4_Data.R def_S4_Model.R utils_S4.R def_S4_DataModelValidation.R
NULL


# GENERIC: Get optim. options ---------------------------------------------
#' Get estimation algorithm options
#'
#' Retrieve model-specific optimization algorithms.
#' @param .model A Model object.
#' @param ... Additional arguments passed to methods.
#' @return A list of options useable for \code{\link{optim}} or \code{\link[nloptr]{nloptr}}.
#' @details This function is called by \link{estimateModel}. Extend this generic to set/change default options.
#' @export
setGeneric('getEstimOpts',
           function(.model, ...) standardGeneric('getEstimOpts'),
           signature = '.model')


# METHOD: Default -------------------------------------------------------------
#' @describeIn getEstimOpts These are the default options.
#' @param verbose A logical, controls the print level in nloptr (0 or 1).
#' @param .overrideAlgorithm NULL or character describing nloptr algorithm to use instead. Will error out if
#' not consistent (i.e. try to use gradient method for function without gradient.)
#' @export
setMethod('getEstimOpts', signature = 'ANY', function(.model, verbose = FALSE, .overrideAlgorithm = NULL) {
  modelOpts <- list(
    #algorithm = 'NLOPT_LN_BOBYQA',
    #algorithm = 'NLOPT_LN_NELDERMEAD',
    algorithm = 'NLOPT_LN_SBPLX',
    xtol_rel = 1e-8,
    maxeval = 1e4,
    print_level = 0 + verbose
    )
  if (not.null(.overrideAlgorithm)) modelOpts$algorithm <- .overrideAlgorithm
  if (verbose) {
    cat(sprintf('No method getEstimOpts for class %s found. Using options:\n', .model@model))
    print(modelOpts)
  }
  modelOpts
})


# METHOD: ModelMDC --------------------------------------------------------
#' @describeIn getEstimOpts An additional set of options that are called when simulating data. In this case analytic
#' gradients are available, so an appropriate algorithm can be employed.
#' @param hasGradient A logical, whether the function has a gradient. Generally passed by calling function.
#' @export
setMethod('getEstimOpts', 'ModelMDC', function(.model, hasGradient = FALSE, verbose = FALSE, .overrideAlgorithm = NULL) {
  modelOpts <- if (hasGradient) list(
    algorithm = 'NLOPT_LD_SLSQP',
    xtol_rel = 1e-6,
    maxeval = 1e3,
    print_level = 0 + verbose
  ) else callNextMethod()
  if (verbose) {
    cat(sprintf('Using getEstimOpts.ModelMDC with%s gradient. Options:\n',
                ifelse(hasGradient, '', 'out')))
    print(modelOpts)
  }
  modelOpts
})

####################################################################################
# GENERIC: ESTIMATION -----------------------------------------------------
#' Estimate Model
#'
#' The super-routine used to estimate a Model associated with a Data.
#' @param .data A Data object.
#' @param .model A Model object.
#' @param ... Additional arguments passed to methods.
#' @return A modified Model object with coefficients set at solved values and ID paired to .data.
#' @details Estimation is performed using nloptr and using options obtained from \link{getEstimOpts}.
#' @export
setGeneric('estimateModel',
           function(.data, .model, ...) standardGeneric('estimateModel'),
           signature = '.model')


# METHOD: ModelSkeleton ---------------------------------------------------
#' @describeIn estimateModel The standard method currently used for all Model-Data pairs. New methods can be written
#' to refine or specialize for particular Models.
#' @param .init A real vector of same total length as coefficients in .model. The start location. Defaults to a random
#' Gaussian spread.
#' @param verbose A logical. Should additional dialogue be printed?
#' @param .overrideNloptrAlgorithm A string corresponding to an nloptr algorithm. Defaults to the result of
#' \code{getEstimOpts}.
setMethod('estimateModel', 'ModelSkeleton', function(.data, .model, .init = NULL,
                                                     verbose = FALSE,
                                                     .overrideNloptrAlgorithm = NULL,
                                                     ...) {
  # Estimate most basic model: SD
  if (verbose) {
    cat(sprintf('Estimating model class %s using nloptr\n', .model@model))
    cat(sprintf('Data size: %s.\n', paste(dim(.data@Z), collapse = ' x ')))
  }
  # Validate that (data, model) pair is consistent
  validateDataModel(.data, .model, skipLikelihoodCheck = TRUE)
  # Define likelihood function in terms of parameters
  ll_fn_par <- function(p) computeLoglikelihood(.data, setCoefs(.model, p), ...) #nLL
  # Get estimation options
  estim_opts <- getEstimOpts(.model, verbose = FALSE, .overrideAlgorithm = .overrideNloptrAlgorithm)
  # Get initial parameters
  npars <- length(getCoefs(.model, doUnlist = TRUE))
  x0 <- if (not.null(.init)) .init else rnorm(npars, sd = sqrt(3))
  if (length(x0) != npars) stop(
    'Initial parameters x0 are incorrect length (.init)',
    call. = FALSE)
  t0 <- Sys.time()
  nloptr_out <- nloptr::nloptr(
    x0 = x0,
    eval_f = ll_fn_par,
    opts = estim_opts
  )
  estim_time <- as.numeric(difftime(Sys.time(), t0, units = 'min'))
  if (verbose) cat(sprintf('Estimation complete in %.02f mins\n', estim_time))

  .model@ID <- .data@ID
  setCoefs(.model, nloptr_out$solution)
})


# Information criterion ---------------------------------------------------
#' Assess fitted model information criterion
#'
#' Compute AIC or BIC from a Data and a Model object fitted from that data.
#' @param .data A Data object.
#' @param .model A Model object.
#' @param type A string, either "AIC" or "BIC"
#' @return The corresponding information criterion.
#' @seealso \url{https://en.wikipedia.org/wiki/Model_selection#Criteria}
#' @export
infoCriterion <- function(.data, .model, type = 'BIC') {
  if (!identical(.data@ID, .model@ID)) warning('Model does not appear to be estimated from provided data')
  N <- nrow(.data@X)
  p <- length(unlist(.model@coefficients))

  LL <- -computeLoglikelihood(.data, .model)

  switch(type,
         AIC = 2*p - 2*LL,
         BIC = log(N)*p - 2*LL)
}

