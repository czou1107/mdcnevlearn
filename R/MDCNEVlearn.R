#' MDCNEVlearn: A package for estimating MDCNEV models with learning
#'
#' @description The MDCNEVlearn package provides a comprehensive set of functions to
#' (1) estimate a suite of marketing models of multiple-discrete continuity in the
#' presence of learning, and (2) simulate data with a set of model parameters
#' @docType package
#' @name MDCNEVlearn
#' @import methods
#' @importFrom graphics matplot
#' @importFrom stats aggregate ave plogis rnorm runif setNames
#' @importFrom utils head relist
NULL
