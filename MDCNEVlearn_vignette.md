---
title: "Introduction to `MDCNEVlearn`"
author: "Zou, Charles"
date: "2019-05-06"
output:
  rmarkdown::html_vignette:
    keep_md: true
vignette: >
  %\VignetteIndexEntry{MDCNEVlearn_vignette}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---



The goal of this document is three-fold: (1) describe the underlying philosophy behind `MDCNEVlearn`, (2) briefly introduce the theory and motivations behind the model, which arise from micro-economic theory but have extensive application outside, and (3) explain the usage of the package. Source code for the package can be found [here](https://bitbucket.org/czou1107/mdcnevlearn).

# Introduction

The primary contribution of this package is to introduce and provide open-source estimation code for the multiple discrete-continuous nested extreme error (MDCNEV) model with Bayesian learning. I developed this particular model as part of my PhD thesis, in order to estimate utility preference and belief parameters for users of a newly released console video game. In its most distilled form, it simply inherits from (1) the class of multiple discrete-continuous models often used in transportation research and environmental economics to understand time allocation and leisure demand, respectively and (2) models of Bayesian (rational) learning employed in quantitative marketing, often to study the demand diffusion of pharmaceuticals or experience goods. In the present framework, I consider consumption/usage as the result of some underlying utility maximization subject to some constraints. Put simply, I assume data $X$ arise from some process:
\begin{align*}
  \max_{x_1,\ldots,x_J} U(x_1,\ldots,x_J) s.t. \sum_j (x_j) \le E
\end{align*}
In fact, this is the central problem considered in economics. Decades of research and many careers have been found in the nuance contained in varying specifications. In this package I consider a sequence of models often studied in quantitative marketing, increasing in complexity and generality to my own, which I will call MDCNEVlearn. I describe this model in slightly more detail later, but the motivation is this:

Consider a situation where consumer behavior is observed over time, say mobile app usage. A consumer may have many apps installed, but only use a handful each week. Consumers are also constantly trying out new apps, and their total time or spent on apps may evolve structurally. How can we predict the drivers of churn, or adoption of a particular app, or whether time spent in one app leads to greater usage of another? Each of these questions seems to suggest a different analysis, but in fact the MDCNEVlearn model can recover deep utility parameters and answer all three. In particular, it naturally accommodates policy analysis of both the extensive (use-or-not-use) and intensive margin (how much usage) in the presence of consumer learning, which is appropriate when consumers are expected to be uncertain about the qualities or their own preferences over goods. Finally, due to the underlying utility structure assumed, this model has no trouble in the presence of sparse data.

## Installation
The development version of this package can be installed using `devtools`:

```r
# install.packages("devtools")
devtools::install_bitbucket("czou1107/MDCNEVlearn")
```

It should then be loadable:

```r
library(MDCNEVlearn)
?MDCNEVlearn
```

# Design philosophy

The library of code written for my thesis contains a complete end-to-end pipeline from raw data to exploratory analysis to model estimation and results. It also includes simulation testing to ensure parameter recovery and many robustness checks for my results. It is extensive but messy, includes proprietary information, and multi-platform: data cleaning and analysis was done in `R`, estimation routines were written in `Matlab` and boosted using the expensive proprietary solver `KNITRO`, and Unix shell scripts were employed to control the estimation process. For all that effort, the shareability was certainly not commensurate. Thus, with this package I hope to make my methdological contribution open-source and easily extensible. After all, no model is one-size-fits-all and even my own, MDCNEVlearn, was driven by the needs of my data.

To that end, each distinct model is implemented as an S4 class and most functions will dispatch from that class. This is particularly useful because almost all of the models implemented nest in some way: parameters are shared, and methods do not need to be written for each model. This also makes extending any particular model straightforward. Additionally, I take the view that when the DGP is correctly specified, the data and model are two sides of the coin: given the set of parameters specifying a model the data are simply one realization, and vice versa. Thus, Model and Data objects are separate and complete on their own.

## Toy example: single discreteness (ModelSD)


The starting point is initializing a Model object, defined by its set of coefficient values. For example, consider the simple case of single discrete-choice: in each period a consumer observes a set of goods $\{1,\ldots, J\}$, each with attributes $z_j$ and has marginal utility $\beta$ over the attributes. Elements of the marketing mix (e.g. price, advertising) may enter into the utility. When consumers select a single good in each purchase occasion, the outcomes $x_j$ are binary and sum to one. This can be modeled with underlying utility:
\begin{align*}
  &U(x_1,\ldots,x_J) = \sum_j u(x_j) \\
  &u(x_j) = \beta_j + z_j'\beta_z + \epsilon_j
\end{align*}
The case where $\epsilon_j$ are assumed i.i.d. Gumbel leads to the standard multinomial logit model. Models of this type are appropriate in product categories specific enough that it is reasonable to expect only a single choice in each occasion. Outside of economics, this latent variable specification is exactly the multinomial logistic regression. In machine learning, for example, $\beta$ are interpreted simply as decision weights.

Given this specification, we may be interested in understanding what associated data looks like--in other words, to sample from this parametric distribution\footnote{Of course, in this example we are sampling from a multinomial distribution which is trivial with `rmultinom`.}. To make this example concrete, say there are two goods $\{1, 2\}$, with baseline marginal utilities $\beta_1 = 1, \beta_2 = -1$ and a single attribute log(price) (coded as `lprice`) with utility weight $-0.5$. This model can be initialized as follows:


```r
toyModel <- ModelSD(coefficients = list(
  beta_c = c(0.3, 1),
  beta_v = c(lprice = -0.5)
))
toyModel
#> Model: SD  (abstract). Utility defined as follows:
#> ----------
#> u(x_j)   ~ (Intercept) + lprice
#> ----------
#> beta     ~ (Intercept) + lprice
#> ----------
#> Parameter values:
#> ----------
#> beta_c  : [(Intercept) = 0.3, 1]
#> beta_v  : [lprice = -0.5]
#> ----------
#> Error distribution: i.i.d. Gumbel (T1EV)
```

`ModelSD` is the class defining single-discreteness, and the coefficients list is comprised to two named vectors. Here I use the `*_c` suffix to indicate intercept terms--it should always be size $J$. `*_v` denotes variable terms (observed heterogeneity, in economics parlance) and should be a named vector corresponding to attributes in the data. To then simulate from this model, we can initialize a Data object:

```r
priceData <- array(rnorm(100), c(50, 2, 1))
simulateData(toyModel, Z = priceData,
             includeOutsideOption = TRUE,
             Z.varNames = 'lprice')
#> Object of class DataSim:
#>   N = 50 unique IDs (1, 2, 3, ...)
#>   NT = 50 total obs. of J = 2 inside goods (+ 1 outside)
#>   M = 1 observed attributes
#> ----------
#> Consumption X (50 x 3):
#>  X1 X2 X0
#>   0  0  1
#>   0  1  0
#>   0  1  0
#>   ...
#>   M = 1 attributes observed for each good
#> ----------
#> Attributes Z (50 x 2 x 1:)
#> lprice:
#>      X1     X2
#>   0.705  0.597
#>  -0.077 -0.938
#>   1.669 -0.514
#>   ...
```


## Toy example: MDCNEVlearn
Finally, we may be given data that we may believe arises from one of the models implemented in the package, and want to estimate its parameters.

Below, I have presented simulated data which is now multiple discrete-continuous. Clearly the model in the previous section could not have generated this data. In particular, linearly separable utility implies consumers will shift their entire consuption to whichever good has the *ex ante* highest MU. Here, the data suggest that there are satiation effects at play, but the binary consume-or-not decision is still relevant--standard utility forms do not allow for corner solutions (zero consumption). In this case we may believe the data arises from the MDCNEV model, and  trends in consumption shares suggest that learning may come into play. Thus, we can estimate two models and compare them as such:

```r
someData
#> Object of class DataSim:
#>   N = 10 unique IDs (a, a, a, ...)
#>   NT = 100 total obs. of J = 2 inside goods (+ 1 outside)
#>   M = 2 observed attributes
#> ----------
#> Consumption X (100 x 3):
#>     X1 X2     X0
#>  9.445  0  0.555
#>  0.000  0 10.000
#>  0.000  0 10.000
#>   ...
#>   M = 2 attributes observed for each good
#> ----------
#> Attributes Z (100 x 2 x 2:)
#> lprice:
#>     X1     X2
#>  0.804 -1.248
#>  0.877  1.403
#>  0.064  1.245
#>   ...
#> ads:
#>      X1     X2
#>   0.188 -0.802
#>  -0.839  0.589
#>  -1.355  1.555
#>   ...
plotData(someData)
```

![plot of chunk 2.estimateData](figure/2.estimateData-1.png)

```r

hypothesisModel1 <- ModelMDCNEV(list(
  beta_v = c(lprice = NA, ads = NA),
  beta_c = numeric(2),
  alpha_c = numeric(2),
  alpha0_c = numeric(1),
  theta_c = numeric(1)
))

hypothesisModel2 <- ModelMDCNEVlearn(list(
  beta_v = c(lprice = NA),
  alpha_c = numeric(2),
  alpha0_c = NA,
  qprior_c = numeric(2),
  qprior_v = c(ads = NA),
  qtrue_c = numeric(2),
  svar_c = numeric(2),
  theta_c = NA
))

set.seed(1010101)
(estimatedModel1 <- estimateModel(someData, hypothesisModel1))
#> Model: MDCNEV estimated from DataSim object. Utility defined as follows:
#> ----------
#> u(x_j)   = 1/alpha_j * Psi_j * [(x_j + 1)^alpha_j - 1]
#> u(x_0)   = 1/alpha_0 * exp(epsilon_0) * x_0^alpha_0
#> ----------
#> Psi_j    ~ exp((Intercept) + lprice + ads)
#> ----------
#> beta     ~ (Intercept) + lprice + ads
#> alpha    ~ 1 - exp((Intercept))
#> alpha0   ~ 1 - exp((Intercept))
#> theta    ~ logistic((Intercept))
#> ----------
#> Parameter values:
#> ----------
#> beta_v  : [lprice = 0.082, ads = -0.106]
#> beta_c  : [(Intercept) = 0.137, 0.007]
#> alpha_c : [(Intercept) = -0.056, 0.691]
#> alpha0_c: [(Intercept) = -1.237]
#> theta_c : [(Intercept) = 1.504]
#> ----------
#> Error distribution: NEV with 0.18 corr. (Kendall's tau) for inside goods
(estimatedModel2 <- estimateModel(someData, hypothesisModel2))
#> Model: MDCNEVlearn estimated from DataSim object. Utility defined as follows:
#> ----------
#> Eu(x_j)  = 1/alpha_j * Psi_j * [(x_j + 1)^alpha_j - 1]
#> u(x_0)   = 1/alpha_0 * exp(epsilon_0) * x_0^alpha_0
#> ----------
#> E(Psi_j) ~ exp(lprice + E(Q_ijt) - ra:Var(Q_ijt))
#> ----------
#> beta     ~ lprice
#> alpha    ~ 1 - exp((Intercept))
#> alpha0   ~ 1 - exp((Intercept))
#> qprior   ~ (Intercept) + ads
#> qtrue    ~ (Intercept)
#> svar     ~ exp((Intercept))
#> theta    ~ logistic((Intercept))
#> ----------
#> Parameter values:
#> ----------
#> beta_v  : [lprice = -0.027]
#> alpha_c : [(Intercept) = 0.05, 0.765]
#> alpha0_c: [(Intercept) = -0.692]
#> qprior_c: [(Intercept) = 3.504, 1.897]
#> qprior_v: [ads = 0.872]
#> qtrue_c : [(Intercept) = -0.634, -1.291]
#> svar_c  : [(Intercept) = -3.412, 1.007]
#> theta_c : [(Intercept) = 1.198]
#> ----------
#> Error distribution: NEV with 0.23 corr. (Kendall's tau) for inside goods
cat(sprintf('Model 1 BIC: %.03f\nModel 2 BIC: %.03f',
            infoCriterion(someData, estimatedModel1),
            infoCriterion(someData, estimatedModel2)))
#> Model 1 BIC: 669.101
#> Model 2 BIC: 659.551
```
To estimate, only the skeleton of a Model object must be provided. Individual coefficients can be `NA`, as long as the coefficient sizes are correct. In this toy example here, we might conclude that the learning model (Model 2) is more appropriate\footnote{Standard errors are a work in progress. In my thesis I use bootstrapped SE. In theory information-based methods should suffice. However, without an analytic Hessian I have found that estimated Hessians do not perform too well in these problems, empirically. See Zou, 2019 for more details.}.

# Models considered

In this package I have currently implemented the single discrete-choice (ModelSD), with Bayesian learning (ModelSDlearn); multiple discrete-choice (ModelMDC), with correlated errors (ModelMDCNEV), and with learning (ModelMDCNEVlearn). 

## Notes

The back-end (computational) implementation of this package currently uses 100% `R` code, without explicit parallelization. Future steps include optimizing computation by computing array-vector/matrix products in `Rcpp`/`RcppArmadillo`, explicit parallelization (`foreach`, `parallel`), and fine-tuning the solver algorithm. This is a continuous work-in-progress, and any comments are greatly welcome.

## References
Zou, C. (2019). *Learning-by-doing and Preference Discovery in Video Game Data*. PhD thesis, University of Chicago Booth School of Business.
